/*

  Adam Majewski
  fraktal.republika.pl

https://gitlab.com/adammajewski/parabolic_chesboard_using_triangle_in_c

  c console progam using 
  * symmetry
  * openMP

  draw  julia sets



  gcc i.c -lm -Wall -fopenmp -march=native 
  time ./a.out
  time ./a.out > info.txt


  How to tune up parameter ? (  )
   
parabolic checkerboard
use target set ( 4 triangles) defined by  points : 
* critical point Zcr=0
* parabolic alfa fixed point Za 
* preimage of alfa = -za
* 2 precritical points :
* + f^{-3} (Zcr) 
* - f^{-3} (Zcr)





*/



#include <stdio.h>
#include <stdlib.h> // malloc
#include <string.h> // strcat
#include <math.h> // M_PI; needs -lm also 
#include <complex.h>
#include <omp.h> // OpenMP; needs also -fopenmp


/* --------------------------------- global variables and consts ------------------------------------------------------------ */
#define iPeriodChild 4 // iPeriodChild of secondary component joined by root point
int iPeriodParent = 1;
// internal angle 
unsigned int numerator = 1;
unsigned int denominator;
double InternalAngle; // numerator/denominator

// virtual 2D array and integer ( screen) coordinate
// Indexes of array starts from 0 not 1 
unsigned int ix, iy; // var
unsigned int ixMin = 0; // Indexes of array starts from 0 not 1
unsigned int ixMax ; //
unsigned int iWidth ; // horizontal dimension of array
unsigned int ixAxisOfSymmetry  ; // 
unsigned int iyMin = 0; // Indexes of array starts from 0 not 1
unsigned int iyMax ; //
unsigned int iyAxisOfSymmetry  ; // 
unsigned int iyAbove ; // var, measured from 1 to (iyAboveAxisLength -1)
unsigned int iyAboveMin = 1 ; //
unsigned int iyAboveMax ; //
unsigned int iyAboveAxisLength ; //
unsigned int iyBelowAxisLength ; //
unsigned int iHeight = 1000; //  odd number !!!!!! = (iyMax -iyMin + 1) = iyAboveAxisLength + iyBelowAxisLength +1
// The size of array has to be a positive constant integer 
unsigned int iSize ; // = iWidth*iHeight; 


// memmory 1D array 
unsigned char *data;
unsigned char *edge;

// unsigned int i; // var = index of 1D array
unsigned int iMin = 0; // Indexes of array starts from 0 not 1
unsigned int iMax ; // = i2Dsize-1  = 
// The size of array has to be a positive constant integer 
// unsigned int i1Dsize ; // = i2Dsize  = (iMax -iMin + 1) =  ;  1D array with the same size as 2D array


/* world ( double) coordinate = dynamic plane */
const double ZxMin=-1.5;
const double ZxMax=1.5;
const double ZyMin=-1.5;
const double ZyMax=1.5;
double PixelWidth; // =(ZxMax-ZxMin)/iXmax;
double PixelWidth2; // =  PixelWidth*PixelWidth;
double PixelHeight; // =(ZyMax-ZyMin)/iYmax;

double ratio ;

// complex numbers of parametr plane 
double Cx; // c =Cx +Cy * i
double Cy;
double complex c; // 

double complex Za; // alfa fixed point alfa=f(alfa)
double Zax, Zay;


double ER = 2.0; // Escape Radius for bailout test 
double ER2;




// points defining target sets 
double Zlx, Zly, Zrx, Zry; 
complex double Zl;

// critical point Zcr
double Zcrx = 0.0;
double Zcry=0.0;



unsigned char  iColorsOfInterior[iPeriodChild]; //={110, 160,210, 223, 240}; // number of colors >= iPeriodChild
static unsigned char iColorOfExterior = 245;
static unsigned char iColorOfUnknown = 80;
unsigned char iJulia = 0;


// unfortunately , because of lazy= slow dynamic
// some points z need very long time to reach attractor 
static unsigned long int iterMax  = 1000000; //iHeight*100;
unsigned int iNumberOfUnknown = 0;

/* ------------------------------------------ functions -------------------------------------------------------------*/

// colors of components interior = shades of gray
int InitColors(int iMax, unsigned char a[])
{
  int i;
  //int iMax = iPeriodChild; iPeriodChild and iColorsOfInterior
  unsigned int iStep;

  iStep=  (iColorOfExterior-2- iColorOfUnknown)/iMax;

  for (i = 1; i <= iMax; ++i)
    {
      a[i-1] = iColorOfExterior -i*iStep; 
      // printf("i= %d color = %i  \n",i-1, iColors[i-1]); // debug
    }
  return 0;
}


/* find c in component of Mandelbrot set 
 
   uses code by Wolf Jung from program Mandel
   see function mndlbrot::bifurcate from mandelbrot.cpp
   http://www.mndynamics.com/indexp.html

*/
double complex GiveC(double InternalAngleInTurns, double InternalRadius, unsigned int Period)
{
  //0 <= InternalRay<= 1
  //0 <= InternalAngleInTurns <=1
  double t = InternalAngleInTurns *2*M_PI; // from turns to radians
  double R2 = InternalRadius * InternalRadius;
  //double Cx, Cy; /* C = Cx+Cy*i */
  switch ( Period ) // of component 
    {
    case 1: // main cardioid
      Cx = (cos(t)*InternalRadius)/2-(cos(2*t)*R2)/4; 
      Cy = (sin(t)*InternalRadius)/2-(sin(2*t)*R2)/4; 
      break;
    case 2: // only one component 
      Cx = InternalRadius * 0.25*cos(t) - 1.0;
      Cy = InternalRadius * 0.25*sin(t); 
      break;
      // for each iPeriodChild  there are 2^(iPeriodChild-1) roots. 
    default: // higher periods : to do, use newton method 
      Cx = 0.0;
      Cy = 0.0; 
      break; }

  return Cx + Cy*I;
}


/*

  http://en.wikipedia.org/wiki/Periodic_points_of_complex_quadratic_mappings
  z^2 + c = z
  z^2 - z + c = 0
  ax^2 +bx + c =0 // ge3neral for  of quadratic equation
  so :
  a=1
  b =-1
  c = c
  so :

  The discriminant is the  d=b^2- 4ac 

  d=1-4c = dx+dy*i
  r(d)=sqrt(dx^2 + dy^2)
  sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx +- sy*i

  x1=(1+sqrt(d))/2 = beta = (1+sx+sy*i)/2

  x2=(1-sqrt(d))/2 = alfa = (1-sx -sy*i)/2

  alfa : attracting when c is in main cardioid of Mandelbrot set, then it is in interior of Filled-in Julia set, 
  it means belongs to Fatou set ( strictly to basin of attraction of finite fixed point )

*/
// uses global variables : 
//  ax, ay (output = alfa(c)) 
double complex GiveAlfaFixedPoint(double complex c)
{
  double dx, dy; //The discriminant is the  d=b^2- 4ac = dx+dy*i
  double r; // r(d)=sqrt(dx^2 + dy^2)
  double sx, sy; // s = sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx + sy*i
  double ax, ay;
 
  // d=1-4c = dx+dy*i
  dx = 1 - 4*creal(c);
  dy = -4 * cimag(c);
  // r(d)=sqrt(dx^2 + dy^2)
  r = sqrt(dx*dx + dy*dy);
  //sqrt(d) = s =sx +sy*i
  sx = sqrt((r+dx)/2);
  sy = sqrt((r-dx)/2);
  // alfa = ax +ay*i = (1-sqrt(d))/2 = (1-sx + sy*i)/2
  ax = 0.5 - sx/2.0;
  ay =  sy/2.0;
 

  return ax+ay*I;
}


/*  gives sign of number */
double sign(double d)
{
  if (d<0)
    {return -1.0;}
  else {return 1.0;};
};



/* mndyncxmics::root from mndyncxmo.cpp  by Wolf Jung (C) 2007-2014. */

// input = x,y
// output = u+v*I = sqrt(x+y*i) 
complex double GiveRoot(complex double z)
{  
  double x = creal(z);
  double y = cimag(z);
  double u, v;
  
   v  = sqrt(x*x + y*y);

   if (x > 0.0)
        { u = sqrt(0.5*(v + x)); v = 0.5*y/u; return  u+v*I; }
   if (x < 0.0)
         { v = sqrt(0.5*(v - x)); if (y < 0.0) v = -v; u = 0.5*y/v; return  u+v*I; }
   if (y >= 0.0) 
       { u = sqrt(0.5*y); v = u; return  u+v*I; }


   u = sqrt(-0.5*y); 
   v = -u;
   return  u+v*I;
}




// from mndlbrot.cpp  by Wolf Jung (C) 2007-2014. part of Madel 5.12 
// input : c, z , mode
// c = cx+cy*i where cx and cy are global variables defined in mndynamo.h
// z = x+y*i
// 
// output : z = x+y*i
complex double InverseIteration(complex double z, complex double c)
{
    double x = creal(z);
    double y = cimag(z);
    double cx = creal(c);
    double cy = cimag(c);
   
   // f^{-1}(z) = inverse with principal value
   if (cx*cx + cy*cy < 1e-20) 
   {  
      z = GiveRoot(x - cx + (y - cy)*I); // 2-nd inverse function = key b 
      //if (mode & 1) { x = -x; y = -y; } // 1-st inverse function = key a   
      return -z;
   }
    
   //f^{-1}(z) =  inverse with argument adjusted
   double u, v;
   complex double uv ;
   double w = cx*cx + cy*cy;
    
   uv = GiveRoot(-cx/w -(cy/w)*I); 
   u = creal(uv);
   v = cimag(uv);
   //
   z =  GiveRoot(w - cx*x - cy*y + (cy*x - cx*y)*I);
   x = creal(z);
   y = cimag(z);
   //
   w = u*x - v*y; 
   y = u*y + v*x; 
   x = w;
   // 2-nd inverse function = key b 
   //if (mode & 1) // mode = -1
     //  { x = -x; y = -y; } // 1-st inverse function = key a
  
  return -x-y*I;

}


// make iPeriod inverse iteration with negative sign ( a in Wolf Jung notation )
complex double GivePrecriticalA( complex double c, int iPeriod)
{
  complex double za = 0.0; // critical point  
  int i; 
  for(i=0;i<iPeriod ;++i){
    
    za = InverseIteration(za,c); 
    //printf("i = %d ,  z = (%f, %f) \n ", i,  creal(z), cimag(z) );

   }

 return za;
}

// http://ncalculators.com/geometry/triangle-area-by-3-points.htm
double GiveTriangleArea(double xa, double ya, double xb, double yb, double xc, double yc)
{
return ((xb*ya-xa*yb)+(xc*yb-xb*yc)+(xa*yc-xc*ya))/2.0;
}

/*

https://en.wikipedia.org/wiki/Curve_orientation
http://mathoverflow.net/questions/44096/detecting-whether-directed-cycle-is-clockwise-or-counterclockwise


The orientation of a triangle (clockwise/counterclockwise) is the sign of the determinant


$
\begin{bmatrix}
1&x_1&y_1\\\\
1&x_2&y_2\\\\
1&x_3&y_3
\end{bmatrix}
$, 


where 
(x_1,y_1), (x_2,y_2), (x_3,y_3)$ 
are the Cartesian coordinates of the three vertices of the triangle.

:<math>\mathbf{O} = \begin{bmatrix}

1 & x_{A} & y_{A} \\
1 & x_{B} & y_{B} \\
1 & x_{C} & y_{C}\end{bmatrix}.</math>

A formula for its determinant may be obtained, e.g., using the method of [[cofactor expansion]]:
:<math>\begin{align}
\det(O) &= 1\begin{vmatrix}x_{B}&y_{B}\\x_{C}&y_{C}\end{vmatrix}
-x_{A}\begin{vmatrix}1&y_{B}\\1&y_{C}\end{vmatrix}
+y_{A}\begin{vmatrix}1&x_{B}\\1&x_{C}\end{vmatrix} \\
&= x_{B}y_{C}-y_{B}x_{C}-x_{A}y_{C}+x_{A}y_{B}+y_{A}x_{C}-y_{A}x_{B} \\
&= (x_{B}y_{C}+x_{A}y_{B}+y_{A}x_{C})-(y_{A}x_{B}+y_{B}x_{C}+x_{A}y_{C}).
\end{align}
</math>

If the determinant is negative, then the polygon is oriented clockwise.  If the determinant is positive, the polygon is oriented counterclockwise.  The determinant  is non-zero if points A, B, and C are non-[[collinear]].  In the above example, with points ordered A, B, C, etc., the determinant is negative, and therefore the polygon is clockwise.

*/

double IsTriangleCounterclockwise(double xa, double ya, double xb, double yb, double xc, double yc)
{return  ((xb*yc + xa*yb +ya*xc) - (ya*xb +yb*xc + xa*yc)); }

int DescribeTriangle(double xa, double ya, double xb, double yb, double xc, double yc)
{
 double t = IsTriangleCounterclockwise( xa,  ya, xb,  yb,  xc,  yc);
 double a = GiveTriangleArea( xa,  ya, xb,  yb,  xc,  yc);
 if (t>0)  printf("this triangle is oriented counterclockwise,     determinent = %f ; area = %f\n", t,a);
 if (t<0)  printf("this triangle is oriented clockwise,            determinent = %f; area = %f\n", t,a);
 if (t==0) printf("this triangle is degenerate: colinear or identical points, determinent = %f; area = %f\n", t,a);

 return 0;
}


int setup()
{

  
  

  denominator = iPeriodChild;
  InternalAngle = numerator/((double) denominator);

  c = GiveC(InternalAngle, 1.0, iPeriodParent) ;
  Cx=creal(c);
  Cy=cimag(c);
  Za = GiveAlfaFixedPoint(c);
  Zax = creal(Za);
  Zay = cimag(Za);
  // 
  // points defining target sets 
  //aaa = z = -0.228890599337287  -0.015109645699267 i
  Zl = GivePrecriticalA(c, iPeriodChild);  //z =    i
  Zlx = creal(Zl); // -0.228890599337287; // // aaa   -0.2288905993372874 ; -0.0151096456992674 
  Zly = cimag(Zl); //-0.015109645699267; //
  Zrx = -Zlx; // aab = 0.229955135116281  +0.141357981605006 i
  Zry = -Zly;
  DescribeTriangle(Zax, Zay, Zrx, Zry,  Zlx, Zly);
  //
  

 

  /* virtual 2D array ranges */
  if (!(iHeight % 2)) iHeight+=1; // it sholud be even number (variable % 2) or (variable & 1)
  iWidth = iHeight;
  iSize = iWidth*iHeight; // size = number of points in array 
  // iy
  iyMax = iHeight - 1 ; // Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].
  iyAboveAxisLength = (iHeight -1)/2;
  iyAboveMax = iyAboveAxisLength ; 
  iyBelowAxisLength = iyAboveAxisLength; // the same 
  iyAxisOfSymmetry = iyMin + iyBelowAxisLength ; 
  // ix
  
  ixMax = iWidth - 1;

  /* 1D array ranges */
  // i1Dsize = i2Dsize; // 1D array with the same size as 2D array
  iMax = iSize-1; // Indexes of array starts from 0 not 1 so the highest elements of an array is = array_name[size-1].


  /* Pixel sizes */
  PixelWidth = (ZxMax-ZxMin)/ixMax; //  ixMax = (iWidth-1)  step between pixels in world coordinate 
  PixelHeight = (ZyMax-ZyMin)/iyMax;
  ratio = ((ZxMax-ZxMin)/(ZyMax-ZyMin))/((float)iWidth/(float)iHeight); // it should be 1.000 ...
  
  

  
 
  /* create dynamic 1D arrays for colors ( shades of gray ) */
  data = malloc( iSize * sizeof(unsigned char) );
  edge = malloc( iSize * sizeof(unsigned char) );
  if (data == NULL || edge == NULL)
    {
      fprintf(stderr," Could not allocate memory\n");
      return 1;
    }
  else fprintf(stderr," memory is OK \n");

  
  // for numerical optimisation 
  ER2 = ER * ER;
  PixelWidth2 =  PixelWidth*PixelWidth;
  

  // fill array iColorsOfInterior with iPeriodChild colors ( shades of gray )
  InitColors(iPeriodChild, iColorsOfInterior);
  
 
  return 0;

}



// from screen to world coordinate ; linear mapping
// uses global cons
double GiveZx(unsigned int ix)
{ return (ZxMin + ix*PixelWidth );}

// uses globaal cons
double GiveZy(unsigned int iy)
{ return (ZyMax - iy*PixelHeight);} // reverse y axis


// ============ http://stackoverflow.com/questions/2049582/how-to-determine-a-point-in-a-2d-triangle
// In general, the simplest (and quite optimal) algorithm is checking on which side of the half-plane created by the edges the point is.
double side (double  x1, double y1, double x2,double y2,double x3, double y3)
{
    return (x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3);
}





// the triangle node numbering is counter-clockwise / clockwise  
int  PointInTriangle (double x, double y, double x1, double y1, double x2, double y2, double x3, double y3)
{
    int  b1, b2, b3;

    b1 = side(x, y, x1, y1, x2, y2) < 0.0;
    b2 = side(x, y, x2, y2, x3, y3) < 0.0;
    b3 = side(x, y, x3, y3, x1, y1) < 0.0;

    return ((b1 == b2) && (b2 == b3));
}


int IfInsideTarget(double Zx, double Zy)
{
 
return PointInTriangle(Zx, Zy, Zax, Zay, Zrx, Zry,  Zlx, Zly)       ;
  
}




unsigned char GiveColor(unsigned int ix, unsigned int iy)
{ 
  // check behavour of z under fc(z)=z^2+c
  // using 2 target set:
  // 1. exterior or circle (center at origin and radius ER ) 
  // as a target set containing infinity = for escaping points ( bailout test)
  // for points of exterior of julia set
  // 2. interior : triangle 


  double Zx2, Zy2;
  int i=0;
  int j=0; // iteration = fc(z)
  
  double Zx, Zy;

  int t=0; // test , boolean value ; 0 = false 
  
  
  
  
  // from screen to world coordinate 
  Zx = GiveZx(ix);
  Zy = GiveZy(iy);
  
  t = IfInsideTarget( Zx, Zy);
  if (t) return iColorsOfInterior[iPeriodChild-1];
    
    

  // if not inside target set around attractor ( alfa fixed point )
 // if not inside target set around attractor ( alfa fixed point )
  while (!t && j<iterMax)
    { // then iterate 
     

    
      for(i=0;i<iPeriodChild ;++i) // iMax = period !!!!
	{  
	  Zx2 = Zx*Zx; 
	  Zy2 = Zy*Zy;
       
	  // bailout test 
	  if (Zx2 + Zy2 > ER2) return iColorOfExterior; // if escaping stop iteration
       
	  // if not escaping or not attracting then iterate = check behaviour
	  // new z : Z(n+1) = Zn * Zn  + C
	  Zy = 2*Zx*Zy + Cy; 
	  Zx = Zx2 - Zy2 + Cx; 
	  //
	  t = IfInsideTarget( Zx, Zy);
	  if (t) return iColorsOfInterior[ i ];
          j+=1;
   
	}
      
      
      
    }

  printf("unknown Z  = %.16f ; %.16f \n", Zx, Zy);
  iNumberOfUnknown +=1;
  return iColorOfUnknown; // 
}


 




/* -----------  array functions -------------- */


/* gives position of 2D point (iX,iY) in 1D array  ; uses also global variable iWidth */
unsigned int Give_i(unsigned int ix, unsigned int iy)
{ return ix + iy*iWidth; }
//  ix = i % iWidth;
//  iy = (i- ix) / iWidth;
//  i  = Give_i(ix, iy);




// plots raster point (ix,iy) 
int PlotPoint(unsigned int ix, unsigned int iy, unsigned char iColor, unsigned char a[])
{
  unsigned i; /* index of 1D array */
  i = Give_i(ix,iy); /* compute index of 1D array from indices of 2D array */
  a[i] = iColor;

  return 0;
}


// fill array 
// uses global var :  ...
// scanning complex plane 
int FillArray(unsigned char a[] )
{
  unsigned int ix, iy; // pixel coordinate 


  // for all pixels of image 
  for(iy = iyMin; iy<=iyMax; ++iy) 
    { printf(" %d z %d\r", iy, iyMax); //info 
      for(ix= ixMin; ix<=ixMax; ++ix) PlotPoint(ix, iy, GiveColor(ix, iy) , a); //  
    } 
   
  return 0;
}


// fill array using symmetry of image 
// uses global var :  ...
int FillArraySymmetric(unsigned char a[] )
{
   
  unsigned char Color; // gray from 0 to 255 

  printf("axis of symmetry \n"); 
  iy = iyAxisOfSymmetry; 
#pragma omp parallel for schedule(dynamic) private(ix,Color) shared(ixMin,ixMax, iyAxisOfSymmetry)
  for(ix=ixMin;ix<=ixMax;++ix) {//printf(" %d from %d\n", ix, ixMax); //info  
    PlotPoint(ix, iy, GiveColor(ix, iy), a);
  }


  /*
    The use of ‘shared(variable, variable2) specifies that these variables should be shared among all the threads.
    The use of ‘private(variable, variable2)’ specifies that these variables should have a seperate instance in each thread.
  */

#pragma omp parallel for schedule(dynamic) private(iyAbove,ix,iy,Color) shared(iyAboveMin, iyAboveMax,ixMin,ixMax, iyAxisOfSymmetry)

  // above and below axis 
  for(iyAbove = iyAboveMin; iyAbove<=iyAboveMax; ++iyAbove) 
    {printf(" %d from %d\r", iyAbove, iyAboveMax); //info 
      for(ix=ixMin; ix<=ixMax; ++ix) 

	{ // above axis compute color and save it to the array
	  iy = iyAxisOfSymmetry + iyAbove;
	  Color = GiveColor(ix, iy);
	  PlotPoint(ix, iy, Color , a); 
	  // below the axis only copy Color the same as above without computing it 
	  PlotPoint(ixMax-ix, iyAxisOfSymmetry - iyAbove , Color , a); 
	} 
    }  
  return 0;
}



// from Source to Destination
int ComputeBoundaries(unsigned char S[], unsigned char D[])
{
 
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */
  /* sobel filter */
  unsigned char G, Gh, Gv; 
  // boundaries are in D  array ( global var )
 
  // clear D array
  memset(D, iColorOfExterior, iSize*sizeof(*D)); // for heap-allocated arrays, where N is the number of elements = FillArrayWithColor(D , iColorOfExterior);
 
  // printf(" find boundaries in S array using  Sobel filter\n");   
#pragma omp parallel for schedule(dynamic) private(i,iY,iX,Gv,Gh,G) shared(iyMax,ixMax, ER2)
  for(iY=1;iY<iyMax-1;++iY){ 
    for(iX=1;iX<ixMax-1;++iX){ 
      Gv= S[Give_i(iX-1,iY+1)] + 2*S[Give_i(iX,iY+1)] + S[Give_i(iX-1,iY+1)] - S[Give_i(iX-1,iY-1)] - 2*S[Give_i(iX-1,iY)] - S[Give_i(iX+1,iY-1)];
      Gh= S[Give_i(iX+1,iY+1)] + 2*S[Give_i(iX+1,iY)] + S[Give_i(iX-1,iY-1)] - S[Give_i(iX+1,iY-1)] - 2*S[Give_i(iX-1,iY)] - S[Give_i(iX-1,iY-1)];
      G = sqrt(Gh*Gh + Gv*Gv);
      i= Give_i(iX,iY); /* compute index of 1D array from indices of 2D array */
      if (G==0) {D[i]=255;} /* background */
      else {D[i]=0;}  /* boundary */
    }
  }
 
   
 
  return 0;
}



// copy from Source to Destination
int CopyBoundaries(unsigned char S[],  unsigned char D[])
{
 
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int i; /* index of 1D array  */
 
 
  //printf("copy boundaries from S array to D array \n");
  for(iY=1;iY<iyMax-1;++iY)
    for(iX=1;iX<ixMax-1;++iX)
      {i= Give_i(iX,iY); if (S[i]==0) D[i]=0;}
 
 
 
  return 0;
}











// Check Orientation of image : first quadrant in upper right position
// uses global var :  ...
int CheckOrientation(unsigned char a[] )
{
  unsigned int ix, iy; // pixel coordinate 
  double Zx, Zy; //  Z= Zx+ZY*i;
  unsigned i; /* index of 1D array */
  for(iy=iyMin;iy<=iyMax;++iy) 
    {
      Zy = GiveZy(iy);
      for(ix=ixMin;ix<=ixMax;++ix) 
	{

	  // from screen to world coordinate 
	  Zx = GiveZx(ix);
	  i = Give_i(ix, iy); /* compute index of 1D array from indices of 2D array */
	  if (Zx>0 && Zy>0) a[i]=255-a[i];   // check the orientation of Z-plane by marking first quadrant */

	}
    }
   
  return 0;
}




int IfInsideTarget2a(double Zx, double Zy)
{
 
return PointInTriangle(Zx, Zy, Zax, Zay,  Zrx, Zry, Zcrx, Zcry)       ;
  
}

int IfInsideTarget2b(double Zx,double Zy)
{
 
return PointInTriangle(Zx, Zy, Zax, Zay, Zcrx, Zcry, Zlx, Zly)       ;
  
}


int IsInside(double Zx, double Zy)
{
  // inside : check 4 triangles 
 // check one triangle and its preimage  
 if  (PointInTriangle(Zx, Zy, Zax, Zay,  Zrx, Zry, Zcrx, Zcry))   return 1;      
 if  (PointInTriangle(Zx, Zy, -Zax, -Zay,  Zlx, Zly, Zcrx, Zcry)) return 1;
 // check one triangle and its preimage
 if  (PointInTriangle(Zx, Zy, Zax, Zay,  Zlx, Zly, Zcrx, Zcry))   return 2;      
 if  (PointInTriangle(Zx, Zy, -Zax, -Zay,  Zrx, Zry, Zcrx, Zcry)) return 2;
 // if it is not in the target then not inside 
 return 0; 

  
 
  
}


// 
unsigned char GiveColor2(unsigned int ix, unsigned int iy)
{ 
  

  double Zx2, Zy2;
  int i=0;
  long int j=0;  
  double Zx, Zy, Zx0, Zy0;
  int flag=0;
  
  
  
  
  
  // from screen to world coordinate 
  Zx0 = GiveZx(ix);
  Zy0 = GiveZy(iy);
  Zx= Zx0;
  Zy = Zy0;
  
  
  flag = IsInside( Zx, Zy);
  if (flag ) 
   { if (flag==1) return iColorsOfInterior[iPeriodChild-1];
     if (flag==2) return iColorsOfInterior[iPeriodChild-1]+11;}
   
    
    

  // if not inside target set around attractor ( alfa fixed point )
   while (!flag && j<iterMax)
    { // then iterate 
     

    
      for(i=0;i<iPeriodChild ;++i) // iMax = period !!!!
	{  
	  Zx2 = Zx*Zx; 
	  Zy2 = Zy*Zy;
       
	  // bailout test 
	  if (Zx2 + Zy2 > ER2) return iColorOfExterior; // if escaping stop iteration
       
	  // if not escaping or not attracting then iterate = check behaviour
	  // new z : Z(n+1) = Zn * Zn  + C
	  Zy = 2*Zx*Zy + Cy; 
	  Zx = Zx2 - Zy2 + Cx; 
	  //
	  flag = IsInside( Zx, Zy);
          if (flag ) 
            { if (flag==1) return iColorsOfInterior[i];
              if (flag==2) return iColorsOfInterior[i]+11;}
        j+=1;
       }
      
       
              
      
    }

  
  
  return iColorOfUnknown; // it should never happen
}







int MakeInternalTilingS(unsigned char a[] )
{
     
  unsigned char Color; // gray from 0 to 255 

  printf("axis of symmetry \n"); 
  iy = iyAxisOfSymmetry; 
#pragma omp parallel for schedule(dynamic) private(ix,Color) shared(ixMin,ixMax, iyAxisOfSymmetry)
  for(ix=ixMin;ix<=ixMax;++ix) {//printf(" %d from %d\n", ix, ixMax); //info  
    PlotPoint(ix, iy, GiveColor2(ix, iy),a);
  }


  /*
    The use of ‘shared(variable, variable2) specifies that these variables should be shared among all the threads.
    The use of ‘private(variable, variable2)’ specifies that these variables should have a seperate instance in each thread.
  */

#pragma omp parallel for schedule(dynamic) private(iyAbove,ix,iy,Color) shared(iyAboveMin, iyAboveMax,ixMin,ixMax, iyAxisOfSymmetry)

  // above and below axis 
  for(iyAbove = iyAboveMin; iyAbove<=iyAboveMax; ++iyAbove) 
    {printf(" %d from %d\r", iyAbove, iyAboveMax); //info 
      for(ix=ixMin; ix<=ixMax; ++ix) 

	{ // above axis compute color and save it to the array
	  iy = iyAxisOfSymmetry + iyAbove;
	  Color = GiveColor2(ix, iy);
	  PlotPoint(ix, iy, Color, a ); 
	  // below the axis only copy Color the same as above without computing it 
	  PlotPoint(ixMax-ix, iyAxisOfSymmetry - iyAbove , Color, a ); 
	} 
    }  
  return 0;
}


// fill array 
// uses global var :  ...
// scanning complex plane 
int MakeInternalTiling(unsigned char a[] )
{
  unsigned int ix, iy; // pixel coordinate 


  // for all pixels of image 
  for(iy = iyMin; iy<=iyMax; ++iy) 
    { printf(" %d z %d\r", iy, iyMax); //info 
      for(ix= ixMin; ix<=ixMax; ++ix) 
           PlotPoint(ix, iy, GiveColor2(ix, iy),a ); //  
    } 
   
  return 0;
}

int DrawCriticalOrbit(unsigned char A[], unsigned int IterMax)
{
 
  unsigned int ix, iy; // pixel coordinate 
  // initial point z0 = critical point
  double Zx=0.0; 
  double Zy=0.0; //  Z= Zx+ZY*i;
  double Zx2=0.0;
  double Zy2=0.0;
  unsigned int i; /* index of 1D array */
  unsigned int j;


  // draw critical point  
  //compute integer coordinate  
  if ( Zx<=ZxMax && Zx>=ZxMin && Zy>=ZyMin && Zy<=ZyMax ){
  ix = (int)((Zx-ZxMin)/PixelWidth);
  iy = (int)((ZyMax-Zy)/PixelHeight); // reverse y axis
  i = Give_i(ix, iy); /* compute index of 1D array from indices of 2D array */
  if (i>=0 && i<iSize) A[i]=255-A[i]; }

  // iterate
  for (j = 1; j <= IterMax; j++) //larg number of iteration s
    {  Zx2 = Zx*Zx; 
      Zy2 = Zy*Zy;
       
      // bailout test 
      if (Zx2 + Zy2 > ER2) return 1; // if escaping stop iteration and return error code
       
      // if not escaping iterate
      // Z(n+1) = Zn * Zn  + C
      Zy = 2*Zx*Zy + Cy; 
      Zx = Zx2 - Zy2 + Cx;
      //compute integer coordinate  
      if ( Zx<=ZxMax && Zx>=ZxMin && Zy>=ZyMin && Zy<=ZyMax )
      {ix = (int)round((Zx-ZxMin)/PixelWidth);
      iy = (int)round((ZyMax-Zy)/PixelHeight); // reverse y axis
      i = Give_i(ix, iy); /* compute index of 1D array from indices of 2D array */
      if (i>=0 && i<iSize) A[i]=255-A[i];   // mark the critical orbit
      }
    }
  return 0;
}





// 
// // scanning complex plane 
int MarkTarget(unsigned char a[] )
{
  unsigned int ix, iy; // pixel coordinate 
  double Zx, Zy; //  Z= Zx+ZY*i;
  unsigned i; /* index of 1D array */
  int flag;


  for(iy=iyMin;iy<=iyMax;++iy) 
    {
      Zy = GiveZy(iy);
      for(ix=ixMin;ix<=ixMax;++ix) 
	{

	  // from screen to world coordinate 
	  Zx = GiveZx(ix);
	  i = Give_i(ix, iy); /* compute index of 1D array from indices of 2D array */
	  //if ( IfInsideTarget2a(Zx,Zy)) data[i]=data[i]-43;
          //if ( IfInsideTarget2b(Zx,Zy)) data[i]=data[i]+43;   // 

          flag = IsInside( Zx, Zy);
          if (flag ) 
           { if (flag==1)  a[i]=a[i] - 43;
              if (flag==2)  a[i]=a[i]+43;}
          
	}
    }
   
  return 0;
}




// save data array to pgm file 
int SaveArray2PGMFile( unsigned char A[], double k, char* comment )
{
  
  FILE * fp;
  const unsigned int MaxColorComponentValue=255; /* color component is coded from 0 to 255 ;  it is 8 bit color file */
  char name [100]; /* name of file */
  snprintf(name, sizeof name, "%.0f", k); /*  */
  char *filename =strncat(name,".pgm", 4);
  
  
  
  /* save image to the pgm file  */      
  fp= fopen(filename,"wb"); /*create new file,give it a name and open it in binary mode  */
  fprintf(fp,"P5\n # %s\n %u %u\n %u\n", comment, iWidth, iHeight, MaxColorComponentValue);  /*write header to the file*/
  fwrite(A,iSize,1,fp);  /*write image data bytes to the file in one step */
  
  //
  printf("File %s saved. ", filename);
  if (comment == NULL)  printf ("empty comment \n");
                   else printf (" comment = %s \n", comment); 
  fclose(fp);

  return 0;
}











int info()
{
  // diplay info messages
  printf("Numerical approximation of parabolic Julia set for complex quadratic polynomial fc(z)= z^2 + c \n");
  printf("parameter c  = %.16f , %.16f \n", Cx, Cy);
  printf("c is a root point between hyperbolic components of period %d and %d  of Mandelbrot set \n", iPeriodParent,  iPeriodChild);
  printf("combinatorial rotation number = internal angle  = %d / %d = %f\n",iPeriodParent,  iPeriodChild, InternalAngle);
  printf("parabolic alfa fixed point Za  = %.16f ; %.16f \n", Zax, Zay);
  printf("image size in pixels = %d x %d \n", iWidth, iHeight);
  printf("image size in world units : (ZxMin = %f, ZxMax =  %f) ,  (ZyMin = %f, ZyMax =  %f) \n", ZxMin , ZxMax ,  ZyMin , ZyMax);
  printf("ratio ( distortion) of image  = %f ; it should be 1.000 ...\n", ratio);
  printf("PixelWidth = %f \n", PixelWidth);
  printf("critical point Zcr  = %.16f ; %.16f \n", Zcrx, Zcry);
  printf("precritical point Zl  = %.16f ; %.16f \n", Zlx, Zly);
  printf("precritical point Zr  = %.16f ; %.16f \n", Zrx, Zry);
  printf("Maximal number of iterations = iterMax = %ld \n", iterMax);
  printf("iNumberOfUknknown  = %d ; It should be zero !!!! \n", iNumberOfUnknown);
  return 0;
}




   


/* -----------------------------------------  main   -------------------------------------------------------------*/
int main()
{
  

  setup();


  // here are procedures for creating image file
  

   
  
  FillArraySymmetric(data); 
  SaveArray2PGMFile(data , 0, "components of filled Julia set"); // save array (image) to pgm file 

  ComputeBoundaries(data, edge);
  SaveArray2PGMFile(edge , 1, "only Julia set"); // save array (image) to pgm file 
  
  DrawCriticalOrbit( data, 1000000);
  SaveArray2PGMFile(data , 2, " components with critical orbit"); // save array (image) to pgm file 

  MarkTarget(data);
  SaveArray2PGMFile(data , 3, "mark target set"); // save array (image) to pgm file

  CopyBoundaries(edge,data);
  SaveArray2PGMFile(data , 4, "with boundaries "); // save array (image) to pgm file 

  MakeInternalTilingS(data);
  SaveArray2PGMFile(data , 5, "Parabolic chessboard = Internal Tiling"); // save array (image) to pgm file 

  ComputeBoundaries(data, edge);
  SaveArray2PGMFile(edge , 6, "only boundaries"); // save array (image) to pgm file 

  CopyBoundaries(edge,data);
  SaveArray2PGMFile(data , 7, " with all boundaries "); // save array (image) to pgm file 

  DrawCriticalOrbit( data, 1000000);
  SaveArray2PGMFile(data , 8, "critical orbit "); // save array (image) to pgm file 

  MarkTarget(data);
  SaveArray2PGMFile(data , 9, "mark target set"); // save array (image) to pgm file

  CheckOrientation(data);
  SaveArray2PGMFile(data , 10, "mark first quadrant "); // save array (image) to pgm file




  free(data);
  free(edge);
  info();
  
  return 0;
}
